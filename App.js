import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Splash from './src/components/splash'
export default class App extends React.Component {

  //ionwillEnter

  render() {
    return (
      <View style={styles.container}>
        <Splash />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderRadius:4,
    borderStyle :'solid',
  }
});
