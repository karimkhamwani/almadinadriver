import React, { Component } from 'react';
import { StyleSheet, Text, View , Image , Button ,  TouchableOpacity} from 'react-native';

class SelectLanguage extends Component {

    onPressLearnMore(){

    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.alMadinaImage}>
                    <Image
                    source={require('../assets/1.png')}
                    />
                </View>

                <View style={styles.starterText}>
                    <Text style={{fontSize:18}}>
                        Water at Your doorstep
                    </Text>
                    <Text style={{color :'#C0C0C0',padding:15,textAlign:'center'}}>
                        With a few taps on the Al Madinah App, get a variety of water products delivered to you in the comfort of your home.
                    </Text>
                </View>

                <View style={styles.orderImage}>
                    <Image
                    style={{flex: 1,    width: null,    height: null,    resizeMode: 'stretch'}}
                    source={require('../assets/02_splash_new.png')}
                    />
                </View>

                <View style={styles.bottomBox}>
                    <View style={{alignItems:'center' , flex: 1}}>
                        <Text style={styles.selectLanguage}>
                            Select Language
                        </Text>
                    </View>
                    <View style={styles.bottomButtons}>
                        <View style={styles.buttonMargin}>
                            <TouchableOpacity
                                style={{backgroundColor :'#0D69AA',height:38,flex:0.6,justifyContent:'center',alignItems:'center'}}
                                onPress={this.onPress}
                            >
                            <Text
                            style={{color:'white'}}> English </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.buttonMargin}>
                            <TouchableOpacity
                                style={{backgroundColor :'#0D69AA',height:38,flex:0.6,justifyContent:'center',alignItems:'center'}}
                                onPress={this.onPress}
                            >
                            <Text
                            style={{color:'white'}}> Arabic </Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>
            </View>
        );
    }
}

export default SelectLanguage;

const styles = StyleSheet.create({
    container : {
        flex : 1
    },
    alMadinaImage : {
        flex : 2,
        justifyContent : 'center',
        alignItems : 'center',
        paddingTop :20
    },
    starterText : {
        flex : 1,
        justifyContent : 'flex-start',
        alignItems : 'center',
        marginTop : 20
    },
    orderImage :{
        backgroundColor : 'green',
        flex : 2
    },
    bottomBox :{
        backgroundColor : '#0D69AA',
        flex : 1,
    },
    selectLanguage: {
        fontSize : 20,
        flex : 1,
        color : '#fff',
        paddingTop:10
    },
    bottomButtons: {
        flex : 2,
        backgroundColor: '#0D69AA',
        alignItems : 'center',
        flexDirection : 'row',
    },
    buttonMargin : {
        marginHorizontal : 10,
        flex :1,
        borderColor :'#fff',
        backgroundColor: '#0D69AA',
        borderWidth: 1,
        borderRadius: 2
    }

})