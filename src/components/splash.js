import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Swiper from 'react-native-swiper';
import SelectLanguage from '../components/selectlanguage'
class Splash extends Component {
  render() {
    return (
      <Swiper style={styles.wrapper}>
        <View style={styles.slide1}>
          <SelectLanguage />
        </View>
        {/*<View style={styles.slide2}>
          <Text style={styles.text}>Beautiful</Text>
        </View>
        <View style={styles.slide3}>
          <Text style={styles.text}>And simple</Text>
        </View>*/}
      </Swiper>
    );
  }
}

export default Splash;

const styles = StyleSheet.create({
  wrapper: {
  },
  slide1: {
    flex: 1
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#97CAE5',
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#92BBD9',
  }
})